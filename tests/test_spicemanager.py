#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Contains SpiceManager tests.
"""

from datetime import datetime

from spice_manager.spicemanager import SpiceManager
from spice_manager.constants import TIME_ISOC_STRFORMAT

from .constants import TEST_DATA_PATH, SOLO_ID
from .test_base import init_spice_manager


def test_spicemanager_instanciation():
    """
    Test SpiceManager class instanciation
    :return:
    """
    spice_manager = SpiceManager()
    assert isinstance(spice_manager, SpiceManager)


def test_spicemanager_singleton():
    """
    Test that SpiceManager Class is a singleton

    :return:
    """
    instance_1 = SpiceManager()
    instance_2 = SpiceManager()
    assert instance_1 == instance_2


def test_spicemanager_init():
    """
    Test initialization of SpiceManager class
    with input list of SPICE kernels

    :return:
    """
    spice_manager = SpiceManager()
    spice_manager.kernels = TEST_DATA_PATH
    assert spice_manager.kernels == TEST_DATA_PATH


@init_spice_manager
def test_spicemanager_unload(spice_manager):
    """
    Test unload() of SpiceManager class
    with input list of SPICE kernels

    :return:
    """
    spice_manager.unload([TEST_DATA_PATH[0]])
    assert spice_manager.kernels == [TEST_DATA_PATH[1]]


@init_spice_manager
def test_spicemanager_unload_all(spice_manager):
    """
    Test unload_all() of SpiceManager class
    with input list of SPICE kernels

    :return:
    """
    spice_manager.unload_all()
    assert spice_manager.kernels == []


@init_spice_manager
def test_spicemanager_kall(spice_manager):
    """
    Test kall() of SpiceManager class
    with input list of SPICE kernels

    :return:
    """
    expected_output = {
        'naif0012.tls':
            [TEST_DATA_PATH[0], 'TEXT', '', 0],
        'solo_ANC_soc-sclk_20221022_V01.tsc':
            [TEST_DATA_PATH[1],
             'TEXT', '', 0]
    }
    assert SpiceManager.kall() == expected_output

def test_spicemanager_cuc2obt():
     """
    Test SpiceManager.cuc2obt() static method

     :return:
     """
     inputs = [712604100, 373]
     expected_outputs = '1/712604100:373'
     assert SpiceManager.cuc2obt(inputs) == expected_outputs

def test_spicemanager_obt2cuc():
     """
    Test SpiceManager.obt2cuc() static method

     :return:
     """
     inputs = '1/712604100:373'
     expected_outputs = (712604100, 373)
     assert SpiceManager.obt2cuc(inputs) == expected_outputs

@init_spice_manager
def test_spicemanager_obt2et(spice_manager):
    """
    Test SpiceManager.obt2et() method

    :return:
    """
    inputs = '1/712604100:373'
    expected_outputs = 712561100.3199744
    assert SpiceManager.obt2et(SOLO_ID, inputs) == expected_outputs

@init_spice_manager
def test_spicemanager_et2obt(spice_manager):
    """
    Test SpiceManager.et2obt() method

    :return:
    """
    inputs = 712561100.3199744
    expected_outputs = '1/0712604100:00373'
    assert SpiceManager.et2obt(SOLO_ID, inputs) == expected_outputs

@init_spice_manager
def test_spicemanager_et2utc(spice_manager):
    """
    Test SpiceManager.et2utc() method

    :return:
    """
    inputs = 712561100.3202845
    expected_outputs = '2022-07-31T17:37:11.137000'
    assert SpiceManager.et2utc(inputs) == expected_outputs

@init_spice_manager
def test_spicemanager_obt2utc(spice_manager):
    """
    Test SpiceManager.obt2utc() method

    :return:
    """
    inputs = '1/0712604100:00373'
    expected_outputs = '2022-07-31T17:37:11.137'
    assert SpiceManager.obt2utc(SOLO_ID, inputs) == expected_outputs

@init_spice_manager
def test_spicemanager_utc2obt(spice_manager):
    """
    Test SpiceManager.utc2obt() method

    :return:
    """
    inputs = '2022-07-31T17:37:11.137'
    expected_outputs = '1/0712604100:00393'
    assert SpiceManager.utc2obt(SOLO_ID, inputs) == expected_outputs

@init_spice_manager
def test_spicemanager_utc2et(spice_manager):
    """
    Test SpiceManager.utc2et() method

    :return:
    """
    inputs = '2022-07-31T17:37:11.137'
    expected_outputs = 712561100.3202845
    assert SpiceManager.utc2et(inputs) == expected_outputs

@init_spice_manager
def test_spicemanager_et2tdt(spice_manager):
    """
    Test SpiceManager.et2tdt() method

    :return:
    """
    inputs = 712561100.3202845
    expected_outputs = 712561100.321
    assert SpiceManager.et2tdt(inputs) == expected_outputs

#     tdt2utc
@init_spice_manager
def test_spicemanager_tdt2utc(spice_manager):
    """
    Test SpiceManager.tdt2utc() method

    :return:
    """
    inputs = 712561100.321
    expected_outputs ='2022-07-31T17:37:11.137000'
    assert SpiceManager.tdt2utc(inputs) == expected_outputs


def test_spicemanager_utc_datetime_to_str():
    """
    Test SpiceManager.utc_datetime_to_str() method

    :return:
    """
    inputs = datetime.strptime('2022-07-31T17:37:11.137000', TIME_ISOC_STRFORMAT)
    expected_outputs = '2022-07-31T17:37:11.137000'
    assert SpiceManager.utc_datetime_to_str(inputs) == expected_outputs

#
def test_spicemanager_utc_str_to_datetime():
    """
    Test SpiceManager.utc_str_to_datetime() method

    :return:
    """
    inputs = '2022-07-31T17:37:11.137000'
    outputs = SpiceManager.utc_str_to_datetime(inputs)
    assert isinstance(outputs, datetime)
    assert outputs.strftime(TIME_ISOC_STRFORMAT) == inputs

@init_spice_manager
def test_spicemanager_cuc2str(spice_manager):
    """
    Test SpiceManager.cuc2str() method

    :return:
    """
    inputs = (712604100, 373)
    expected_outputs = '712604100:373'
    assert spice_manager.cuc2str(inputs[0], inputs[1]) == expected_outputs

@init_spice_manager
def test_spicemanager_cuc2datetime(spice_manager):
    """
    Test SpiceManager.cuc2datetime() method

    :return:
    """
    inputs = (712604100, 373)
    expected_outputs = '2022-07-31T17:35:00.005692'
    outputs = spice_manager.cuc2datetime(inputs[0], inputs[1])
    assert isinstance(outputs, datetime)
    assert outputs.strftime(TIME_ISOC_STRFORMAT) == expected_outputs

#
