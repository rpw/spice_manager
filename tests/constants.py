# -*- coding: utf-8 -*-

from pathlib import Path

from spice_manager.constants import NAIF_ID


BASE_DIR = Path(__file__).resolve().parent / 'data'


# Get Solar Orbiter NAIF SPICE ID
SOLO_ID = NAIF_ID['SOLAR_ORBITER']

# Get test data paths
TEST_DATA_PATH = [
    str(BASE_DIR / 'naif0012.tls'),
    str(BASE_DIR / 'solo_ANC_soc-sclk_20221022_V01.tsc'),
]
